# Generated by Django 2.0.6 on 2018-06-16 15:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('part', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='part',
            name='third_prop',
            field=models.BooleanField(default=False),
        ),
    ]

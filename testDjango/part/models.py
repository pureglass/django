from django.db import models
from django.urls import reverse

class Part(models.Model):
    first_prop = models.CharField(max_length=255)
    second_prop = models.CharField(max_length=250)
    third_prop = models.BooleanField(default=False)
    img_link = models.FileField()

    def get_absolute_url(self):
        return reverse('part:part_detail', kwargs={'pk' : self.pk})

    def __str__(self):
        return self.first_prop + ' - ' + self.second_prop

class Next(models.Model):
    part = models.ForeignKey(Part, on_delete=models.CASCADE)
    file_type = models.CharField(max_length=10)
    next_title = models.CharField(max_length=252)

    def __str__(self):
        return self.file_type
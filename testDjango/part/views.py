from django.views import generic
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from .models import Part
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from django.views.generic import View
from .forms import UserForm

class IndexView(generic.ListView):
    template_name = 'part/index.html'
    context_object_name = 'part_list'

    def get_queryset(self):
        return Part.objects.all()

class DetailView(generic.DetailView):
    model = Part
    template_name = 'part/detail.html'

class PartCreate(CreateView):
    model = Part
    fields = [
        'first_prop',
        'second_prop',
        'third_prop',
        'img_link'
    ]

class PartUpdate(UpdateView):
    model = Part
    fields = [
        'first_prop',
        'second_prop',
        'third_prop'
    ]

class PartDelete(DeleteView):
    model = Part
    success_url = reverse_lazy('part:index')

class UserFormView(View):
    form_class = UserForm
    template_name = 'part/registration_form.html'

    # display form
    def get(self, request):
        form = self.form_class(None)
        return render(request, self.template_name, {'form':form})

    # register user
    def post(self, request):
        form = self.form_class(request.POST)

        if form.is_valid():

            user = form.save(commit=False)

            # cleaned data
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user.set_password(password)
            user.save()

            user = authenticate(username=username, password=password)

            if user is not None:
                if user.is_active:
                    login(request, user)
                    return redirect('part:index')

        return render(request, self.template_name, {'form': form})


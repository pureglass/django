from django.urls import path
from . import views
from django.conf.urls import url

app_name = 'part'

urlpatterns = [

    path('', views.IndexView.as_view() , name='index'),
    path('<int:pk>/', views.DetailView.as_view() , name='part_detail'),
    path('add/', views.PartCreate.as_view(), name="part_add"),
    path('register/', views.UserFormView.as_view(), name="register"),
    path('update/<int:pk>/', views.PartUpdate.as_view(), name="part_update"),
    path('delete/<int:pk>/', views.PartDelete.as_view(), name="part_delete")

]
from django.contrib import admin
from .models import Part, Next

admin.site.register(Part)
admin.site.register(Next)

import difflib
import Levenshtein

s = ' '
a = 'MAXI COSI vežimėlis Nova 4wheels NOVA 4 wheels Nomand Grey'
a = a.split()
a.sort()
a = s.join(a)

b = 'Vežimėlis Maxi Cosi Nova 4 (2 in 1) su lopšiu Oria Nomad Grey'
b = b.split()
b.sort()
b = s.join(b)

seq=difflib.SequenceMatcher(a=a.lower(), b=b.lower())
print(seq.ratio())

lev = Levenshtein.ratio(a, b)
print(lev)
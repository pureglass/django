# Generated by Django 2.0.6 on 2018-06-22 08:33

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('compare', '0006_auto_20180622_0731'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProductRelation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('product', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='product', to='compare.Product')),
                ('related_product', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='related_product', to='compare.Product')),
            ],
        ),
    ]

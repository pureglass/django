from django.db.models import Count
from django.shortcuts import render
from .models import Product, Shop


def index(request):
    products = Product.objects\
        .filter(title__icontains='vezimelis')


    # dups = (
    #     Product.objects
    #         .filter(title__icontains='vezimelis')
    #         .values('title')
    #         .annotate(bsnbb
    #         .count=Count('id'))
    #         .values('title')
    #         .order_by()
    #         .filter(count__gt=1)
    # )
    #
    # products = Product.objects.filter(title__in=dups)

    # products = Product.objects.raw('''
    #     SELECT id, title
    #     FROM compare_product
    #     GROUP BY title
    #     HAVING COUNT(*) > 1
    #     AS same;
    #
    #
    # ''')

    products = Product.objects.raw('''
        
    ''')

    # products_shop_list = Product.objects.filter(title__icontains='vezimelis').values('shop').distinct()
    #
    # shop_names = Shop.objects.all().values('id')

    context = {
        'products': products,
        # 'products_shop_list': products_shp_list,
        # 'shop_names': shop_names,
        # 'product_search': request.GET.get('product_search', '')
    }

    return render(request, 'compare/index.html', context)

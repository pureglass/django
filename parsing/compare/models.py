from django.db import models
import django_filters

class Shop(models.Model):
    title = models.CharField(max_length=20)
    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

class Product(models.Model):
    uuid = models.UUIDField(editable=False, unique=True)
    title = models.CharField(max_length=500)
    price = models.CharField(max_length=255)
    item_link = models.CharField(max_length=1000)
    shop = models.ForeignKey(Shop, on_delete=models.CASCADE)
    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

class ProductRelation(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='product', blank=True, null=True)
    related_product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='related_product', blank=True, null=True)
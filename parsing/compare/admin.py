from django.contrib import admin
from .models import Shop, Product

class ShopModelAdmin(admin.ModelAdmin):
    pass

class ProductModelAdmin(admin.ModelAdmin):
    pass

# class RelateModelAdmin(admin.ModelAdmin):
#     pass


admin.site.site_header = 'Admin Panel'
admin.site.index_title = 'Price Grab'
admin.site.index_template = 'admin/compare/index.html'
admin.autodiscover()

admin.site.register(Shop, ShopModelAdmin)
admin.site.register(Product, ProductModelAdmin)
# admin.site.register(RelateProduct, RelateModelAdmin)
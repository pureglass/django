from django.core.management.base import BaseCommand, CommandError
from parsing.compare.models import Product, ProductRelation, Shop
import difflib

class RelateProducts(BaseCommand):

    def fill_up_rel_table(self, apps):
        shop_ids = Shop.objects.all().values('id')

        for id in shop_ids:
            for product in Product.objects.filter(shop_id=id):
                for id in shop_ids:
                    for product in Product.objects.filter(shop_id=id):

                        product.save()



    # def find_related(self):
    #     s = ' '
    #     a = 'MAXI COSI vežimėlis Nova 4wheels NOVA 4 wheels Nomand Grey'
    #     a = a.split()
    #     a.sort()
    #     a = s.join(a)
    #
    #     b = 'Vežimėlis Maxi Cosi Nova 4 (2 in 1) su lopšiu Oria Nomad Grey'
    #     b = b.split()
    #     b.sort()
    #     b = s.join(b)
    #
    #     seq = difflib.SequenceMatcher(a=a.lower(), b=b.lower())

    def add_arguments(self, parser):
        parser.add_argument('poll_id', nargs='+', type=int)

    def handle(self, *args, **options):
        for poll_id in options['poll_id']:
            try:
                poll = Product.objects.get(pk=poll_id)
            except Product.DoesNotExist:
                raise CommandError('Poll "%s" does not exist' % poll_id)

            poll.opened = False
            poll.save()

            self.stdout.write(self.style.SUCCESS('Successfully closed poll "%s"' % poll_id))